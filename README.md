# vue-table-demo

## About - 关于
基于Vue2.*的自定义表格组件  
效果图  
![avatar](/public/Animation.gif)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
